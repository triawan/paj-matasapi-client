<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Person */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'People', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="person-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'nik',
            'prefix_title',
            'first_name',
            'middle_name',
            'last_name',
            'suffix_title',
            'phonetic_name',
            'gender_id',
            'place_of_birth',
            'date_of_birth',
            'address_id',
            'contact_id',
            'body_weight',
            'body_height',
            'blood_type_id',
            'marital_status_id',
            'emergency_name',
            'emergency_contact',
            'emergency_address',
            'family_card_id',
            'insurance_id',
            'bank_account_id',
            'npwp',
            'photo_source',
            'personal_website',
            'primary_email:email',
            'secondary_email:email',
            'is_deleted',
            'updated_at',
        ],
    ]) ?>

</div>
