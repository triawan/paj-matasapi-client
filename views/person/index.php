<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\PersonSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'People';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="person-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Person', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'nik',
            'prefix_title',
            'first_name',
            'middle_name',
            //'last_name',
            //'suffix_title',
            //'phonetic_name',
            //'gender_id',
            //'place_of_birth',
            //'date_of_birth',
            //'address_id',
            //'contact_id',
            //'body_weight',
            //'body_height',
            //'blood_type_id',
            //'marital_status_id',
            //'emergency_name',
            //'emergency_contact',
            //'emergency_address',
            //'family_card_id',
            //'insurance_id',
            //'bank_account_id',
            //'npwp',
            //'photo_source',
            //'personal_website',
            //'primary_email:email',
            //'secondary_email:email',
            //'is_deleted',
            //'updated_at',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
