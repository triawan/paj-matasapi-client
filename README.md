<p align="center">
    <a href="https://github.com/yiisoft" target="_blank">
        <img src="https://avatars0.githubusercontent.com/u/993323" height="100px">
    </a>
    <h2 align="center">Yii 2 Basic Project Matasapi Sebagai Client</h2>
    <br>
</p>


Pembuatan Client untuk akses API yang disediakan oleh project Mata Sapi

Tim CSN :

Agus Mulyana - G6601211014
<br>
Defiana Arnaldy - G6601211005
<br>
Sopian Alviana - G6601211024
<br>
Triawan Adi Cahyanto - G6601211016

FITUR LAYANAN
-------------

Tidak ada fitur layanan, hanya ada fitur untuk CRUD terkait data person. Hal ini dapat diakses pada url:
~~~
http://localhost/paj-matasapi-client/web/person
~~~

DIRECTORY STRUCTURE
-------------------

      assets/             contains assets definition
      commands/           contains console commands (controllers)
      config/             contains application configurations
      controllers/        contains Web controller classes
      mail/               contains view files for e-mails
      models/             contains model classes
      runtime/            contains files generated during runtime
      tests/              contains various tests for the basic application
      vendor/             contains dependent 3rd-party packages
      views/              contains view files for the Web application
      web/                contains the entry script and Web resources

REQUIREMENTS
------------

The minimum requirement by this project template that your Web server supports PHP 5.6.0.


INSTALLATION
------------

### Install via Composer

If you do not have [Composer](http://getcomposer.org/), you may install it by following the instructions
at [getcomposer.org](http://getcomposer.org/doc/00-intro.md#installation-nix).

You can then install this project template using the following command:

~~~
composer create-project --prefer-dist yiisoft/yii2-app-basic paj-matasapi-client
~~~

Now you should be able to access the application through the following URL, assuming `paj-matasapi` is the directory
directly under the Web root.

~~~
http://localhost/paj-matasapi-client/web/
~~~

### Install from an Archive File

Extract the archive file downloaded from [yiiframework.com](http://www.yiiframework.com/download/) to
a directory named `basic` that is directly under the Web root.

Set cookie validation key in `config/web.php` file to some random secret string:

```php
'request' => [
    // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
    'cookieValidationKey' => '<secret random string goes here>',
],
```

You can then access the application through the following URL:

~~~
http://localhost/paj-matasapi-client/web/
~~~

CONFIGURATION
-------------

### Database

Tidak menggunakan database, melainkan hanya mengakses API yang disediakan oleh API Gateway (dalam hal ini, paj-matasapi sebagai layanan penyedia API)

UJICOBA
-------
### Akses API 
Penampakan ujicoba ada pada gambar berikut ini:

![](https://gitlab.com/triawan/paj-matasapi-client/-/raw/main/assets/akses-api-person.PNG)
<figcaption>Gambar Akses data API dari project paj-matasapi (API Gateway)</figcaption>
<br>
