<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Person;

/**
 * PersonSearch represents the model behind the search form of `app\models\Person`.
 */
class PersonSearch extends Person
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'gender_id', 'address_id', 'contact_id', 'blood_type_id', 'marital_status_id', 'family_card_id', 'insurance_id', 'bank_account_id', 'is_deleted'], 'integer'],
            [['nik', 'prefix_title', 'first_name', 'middle_name', 'last_name', 'suffix_title', 'phonetic_name', 'place_of_birth', 'date_of_birth', 'emergency_name', 'emergency_contact', 'emergency_address', 'npwp', 'photo_source', 'personal_website', 'primary_email', 'secondary_email', 'updated_at'], 'safe'],
            [['body_weight', 'body_height'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Person::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'gender_id' => $this->gender_id,
            'date_of_birth' => $this->date_of_birth,
            'address_id' => $this->address_id,
            'contact_id' => $this->contact_id,
            'body_weight' => $this->body_weight,
            'body_height' => $this->body_height,
            'blood_type_id' => $this->blood_type_id,
            'marital_status_id' => $this->marital_status_id,
            'family_card_id' => $this->family_card_id,
            'insurance_id' => $this->insurance_id,
            'bank_account_id' => $this->bank_account_id,
            'is_deleted' => $this->is_deleted,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'nik', $this->nik])
            ->andFilterWhere(['like', 'prefix_title', $this->prefix_title])
            ->andFilterWhere(['like', 'first_name', $this->first_name])
            ->andFilterWhere(['like', 'middle_name', $this->middle_name])
            ->andFilterWhere(['like', 'last_name', $this->last_name])
            ->andFilterWhere(['like', 'suffix_title', $this->suffix_title])
            ->andFilterWhere(['like', 'phonetic_name', $this->phonetic_name])
            ->andFilterWhere(['like', 'place_of_birth', $this->place_of_birth])
            ->andFilterWhere(['like', 'emergency_name', $this->emergency_name])
            ->andFilterWhere(['like', 'emergency_contact', $this->emergency_contact])
            ->andFilterWhere(['like', 'emergency_address', $this->emergency_address])
            ->andFilterWhere(['like', 'npwp', $this->npwp])
            ->andFilterWhere(['like', 'photo_source', $this->photo_source])
            ->andFilterWhere(['like', 'personal_website', $this->personal_website])
            ->andFilterWhere(['like', 'primary_email', $this->primary_email])
            ->andFilterWhere(['like', 'secondary_email', $this->secondary_email]);

        return $dataProvider;
    }
}
