<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "person".
 *
 * @property int $id
 * @property string $nik
 * @property string|null $prefix_title
 * @property string|null $first_name
 * @property string|null $middle_name
 * @property string|null $last_name
 * @property string|null $suffix_title
 * @property string|null $phonetic_name
 * @property int|null $gender_id
 * @property string|null $place_of_birth
 * @property string|null $date_of_birth
 * @property int|null $address_id
 * @property int|null $contact_id
 * @property float|null $body_weight
 * @property float|null $body_height
 * @property int|null $blood_type_id
 * @property int|null $marital_status_id
 * @property string|null $emergency_name
 * @property string|null $emergency_contact
 * @property string|null $emergency_address
 * @property int|null $family_card_id
 * @property int|null $insurance_id
 * @property int|null $bank_account_id
 * @property string|null $npwp
 * @property string|null $photo_source
 * @property string|null $personal_website
 * @property string|null $primary_email
 * @property string|null $secondary_email
 * @property int|null $is_deleted
 * @property string|null $updated_at
 */
class Person extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'person';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'nik'], 'required'],
            [['id', 'gender_id', 'address_id', 'contact_id', 'blood_type_id', 'marital_status_id', 'family_card_id', 'insurance_id', 'bank_account_id', 'is_deleted'], 'integer'],
            [['date_of_birth', 'updated_at'], 'safe'],
            [['body_weight', 'body_height'], 'number'],
            [['nik', 'prefix_title', 'suffix_title', 'phonetic_name', 'npwp'], 'string', 'max' => 30],
            [['first_name', 'middle_name', 'last_name', 'emergency_name'], 'string', 'max' => 50],
            [['place_of_birth'], 'string', 'max' => 40],
            [['emergency_contact'], 'string', 'max' => 15],
            [['emergency_address'], 'string', 'max' => 100],
            [['photo_source'], 'string', 'max' => 120],
            [['personal_website', 'primary_email', 'secondary_email'], 'string', 'max' => 80],
            [['nik'], 'unique'],
            [['id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nik' => 'Nik',
            'prefix_title' => 'Prefix Title',
            'first_name' => 'First Name',
            'middle_name' => 'Middle Name',
            'last_name' => 'Last Name',
            'suffix_title' => 'Suffix Title',
            'phonetic_name' => 'Phonetic Name',
            'gender_id' => 'Gender ID',
            'place_of_birth' => 'Place Of Birth',
            'date_of_birth' => 'Date Of Birth',
            'address_id' => 'Address ID',
            'contact_id' => 'Contact ID',
            'body_weight' => 'Body Weight',
            'body_height' => 'Body Height',
            'blood_type_id' => 'Blood Type ID',
            'marital_status_id' => 'Marital Status ID',
            'emergency_name' => 'Emergency Name',
            'emergency_contact' => 'Emergency Contact',
            'emergency_address' => 'Emergency Address',
            'family_card_id' => 'Family Card ID',
            'insurance_id' => 'Insurance ID',
            'bank_account_id' => 'Bank Account ID',
            'npwp' => 'Npwp',
            'photo_source' => 'Photo Source',
            'personal_website' => 'Personal Website',
            'primary_email' => 'Primary Email',
            'secondary_email' => 'Secondary Email',
            'is_deleted' => 'Is Deleted',
            'updated_at' => 'Updated At',
        ];
    }
}
